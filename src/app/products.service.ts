import { Injectable } from '@angular/core';
import { HttpClient, HttpParams} from '@angular/common/http';
import { product } from 'src/datamodel/product';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  Products:product[] = new Array;
  observedProducts:Observable<product[]>;
  serverURL = 'assets/data/products.json';
  constructor(private htpData:HttpClient) {
    this.observedProducts = this.htpData.get<product[]>(this.serverURL);
    this.observedProducts.subscribe(data=> this.Products = data)
   }

   getProduct(productID:string){
     let params = new HttpParams().set("productId",productID)
     return this.htpData.get<product[]>(this.serverURL);
   }
}
