import { Component} from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { product } from 'src/datamodel/product';
import { ProductsService } from '../products.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent{
  SelectedProduct:product
  constructor(public prods:ProductsService) {
    this.SelectedProduct =   {
      productId: 10,
      productName: "",
      productCode: "",
      releaseDate: new Date,
      description: "",
      price: 0,
      starRating: 0,
      imageUrl: ""
    }
    prods.getProduct("2").subscribe(data => {this.SelectedProduct = data[2]
    console.log(data)});
  }
}
