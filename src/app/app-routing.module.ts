import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';

const routes: Routes = [
  {
    path:'',
    loadChildren:() => import('./home/home.module').then(m=>m.HomeModule),
    component:HomeComponent
  },
  {
    path:'products',
    loadChildren:() => import('./products/products.module').then(m=>m.ProductsModule),
    component:ProductsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
