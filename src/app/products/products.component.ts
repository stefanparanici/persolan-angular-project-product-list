import { Component} from '@angular/core';
import { ProductsService } from '../products.service';
import { DatePipe } from '@angular/common';
import { product } from 'src/datamodel/product';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent {
  prodsDB:product[];
  datepipe: DatePipe;
  searchBar:string;
  showImages:boolean;
  imagesButton:string;
  constructor(public route: ActivatedRoute,public prods:ProductsService){
    this.datepipe = new DatePipe('en-US');
    this.searchBar = "";
    this.prodsDB = this.prods.Products;
    this.showImages = false;
    this.imagesButton = "Show Products";
  }

  ToggleImages(){
    this.showImages = !this.showImages;
    this.imagesButton = this.showImages ? "Hide Poducts":"Show Products";
  }
  
  Search(){
    this.prods.Products = this.prodsDB.filter(res=>{
      return res.productName.toLocaleLowerCase().includes(this.searchBar.toLocaleLowerCase());
    })
  }
}
