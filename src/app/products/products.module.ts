import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ProductsRoutingModule } from './products-routing.module';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    ProductsComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ProductsRoutingModule,
    HttpClientModule
  ]
})
export class ProductsModule { }